/* Setup global settings */
app.factory('settings', ['$rootScope', function ($rootScope) {
        // supported languages

        var settings = {
            layout: {
                pageSidebarClosed: false, // sidebar menu state
                pageSidebarFixed: false, // sidebar fixa
                pageContentWhite: true, // set page content layout
                pageBodySolid: true, // solid body color state
                pageAutoScrollOnLoad: 1000, // auto scroll to top on page load
                containerFluid: true,
                menuUsuario: true,
            },
            assetsPath: '../assets',
            globalPath: '../assets/global',
            layoutPath: '../assets/layouts/layout3',
            menu: [
                //{menu: "eCompras", icon: "icon-inbox", start: true, state: "#/{{dashboard}}"},
                // {menu: "Publicações", icon: "", itens: [
                //         {menu: "Nivel Hierarquico", icon: "icon-list", state: "#/tabela/Nivel/listar"},
                //         {menu: "Orgão/Unidade Administrativa", icon: "icon-list", state: "#/tabela/UnidadeAdministrativa/listar"},
                //         {menu: "Responsável", icon: "icon-list", state: "#/tabela/Responsavel/listar"},
                //         // {menu: "UA", icon: "icon-list", state: "#/UnidadeAdministrativa"},
                //         //{menu: "Clientes", icon: "icon-list", click: true, entity: "unidade"},
                //         //{menu: "Clientes", icon: "icon-list", click: true, entity: "cliente"},
                //     ]},
                {menu: "Tabelas", icon: "", itens: [
                        {menu: "Tipo Documento", icon: "icon-list", state: "#/tabela/TipoDocumento/listar"}
                        //{menu: "Responsável", icon: "icon-list", state: "#/tabela/Responsavel/listar"},
                        // {menu: "UA", icon: "icon-list", state: "#/UnidadeAdministrativa"},
                        //{menu: "Clientes", icon: "icon-list", click: true, entity: "unidade"},
                        //{menu: "Clientes", icon: "icon-list", click: true, entity: "cliente"},
                    ]},
                {menu: "Documentos", icon: "", state: "#/tabela/DocumentoDocam/listar"},
                // {menu: "Relatórios", icon: "", itens: [
                //         {menu: "Documentos Publicados", icon: "", state: "#/tabela/InteressadoInterno/listar"},
                //         {menu: "Documentos Não Publicados", icon: "", state: "#/tabela/InteressadoExterno/listar"}
                //     ]},
                // {menu: "Publicações", icon: "", state: "#/tabela/DocumentoOficial/listar"},
            ]
        };
    $rootScope.settings = settings;

    return settings;
}]);
