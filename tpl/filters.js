app.module('SimplesApp', [ ]);
app.module('SimplesApp').filter('dateFormat', function($filter)
{
  return function(input)
  {
    if(input === null){ return ""; }
    var _date = $filter('date')(new Date(input), 'MMM dd yyyy');
    return _date.toUpperCase();
  };
});

app.module('SimplesApp').filter('dateFormat1', function($filter)
{
  return function(input)
  {
    if(input === null){ return ""; }
    var _date = $filter('date')(new Date(input), 'MM dd yyyy');
    return _date.toUpperCase();
  };
});

app.module('SimplesApp').filter('time', function($filter)
{
  return function(input)
  {
    if(input === null){ return ""; }
    var _date = $filter('date')(new Date(input), 'HH:mm:ss');
    return _date.toUpperCase();
  };
});

app.module('SimplesApp').filter('dataAtual', function($filter)
{
  return function(input)
  {
    if(input === null){ return ""; }
    var _date = $filter('date')(new Date(input),
    'dd/MM/yyyy');
    return _date.toUpperCase();
  };
});
app.module('SimplesApp').filter('datetime1', function($filter)
{
  return function(input)
  {
    if(input === null){ return ""; }
    var _date = $filter('date')(new Date(input),
    'MM dd yyyy' + 'as' + 'HH:mm:ss');
    return _date.toUpperCase();
  };
});


//Filter CPF
app.module('SimplesApp').filter('cpf', function($filter){
  return function(input) {
      var str = input + '';
      if(str.length <= 11){
        str = str.replace(/\D/g, '');
        str = str.replace(/(\d{3})(\d)/, "$1.$2");
        str = str.replace(/(\d{3})(\d)/, "$1.$2");
        str = str.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
      }
      return str;
    };
});

//Filter CNPJ
app.module('SimplesApp').filter('cnpj', function ($filter){
  return function(input){
    var str = input + '';
    if(str.length <= 14){
      str = str.replace(/\D/g, '');
      str = str.replace(/(\d{2})(\d)/, "$1.$2");
      str = str.replace(/(\d{3})(\d)/, "$1.$2");
      str = str.replace(/(\d{4})(\d)/, "$1/$2");
      str = str.replace(/(\d{2})(\d{1,2})$/, "$1-$2");
    }
    return str;
  };
});

//Filter SEXO
app.module('SimplesApp').filter('sexo', function ($filter){
  return function(input){
    var str = input + '';
    if(str == "F" || str == "f"){
      str = "Feminino";
    }else{
      str = "Masculino";
    }
    return str;
  };
});

//Filter Sim/Nao
app.module('SimplesApp').filter('simNaoTrueFalse', function ($filter){
  return function(input){
    var str = input + '';
    if(str == "S" || str == "s" || str == 'TRUE' || str == 'true'){
      str = '<span class="label label-sm label-success block btn-circle">Sim</span>';
    }else{
      str = '<span class="label label-sm label-danger block btn-circle">Não</span>';//"Não";
    }
    return str;
  };
});

//Filter Mes Extenso
app.module('SimplesApp').filter('mesExtenso', function ($filter){
  return function(input){
    var str = input + '';
    if(str == "S" || str == "s" || str == 'TRUE' || str == 'true'){
      str = '<span class="label label-sm label-success block">Sim</span>';
    }else{
      str = '<span class="label label-sm label-danger block">Não</span>';//"Não";
    }
    return str;
  };
});
