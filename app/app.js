
var app = angular.module("SimplesApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngMessages",
    "ngSanitize",
    "si.core",
    "si.datasource",
    "si.grid",
    "si.lookup",
    "si.selectbox",
    "si.schema",
    "si.auth",
    "si.dialog",
    "si.utils",
    "xdata-angular",
    "dx",
    //"ui.tinymce",
    "ckeditor",
]);


/* Setup Layout Part - Header */
app.controller('HeaderController', ['$scope', '$rootScope', 'AuthService', 'AUTH_EVENTS', function($scope, $rootScope, AuthService, AUTH_EVENTS) {

    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });

    $scope.nomeUsuario = AuthService.getUser().nome;
    //console.log(AuthService.getUser().nome);

    $scope.logout = function() {
        AuthService.logout();
    };
}]);

/* Setup Layout Part - Sidebar */
app.controller('PageHeadController', ['$scope', function($scope) {

    $scope.$on('$includeContentLoaded', function() {

    });
}]);

/* Setup Layout Part - Footer */
app.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });

}]);

/* Setup App Main Controller */
app.controller('ApplicationController', ['$compile', '$q', '$scope', '$rootScope', '$state', '$window', 'AuthService', 'AUTH_EVENTS', 'Utils', 'XDataClient', 'AppConfig', 'Dialog', 'Scopes', 'SimplesGrid', 'XDataSource', 'AcessoService', 'Redirect',  'SimplesSelectBox',
    function($compile, $q, $scope, $rootScope, $state, $window, AuthService, AUTH_EVENTS, Utils, XDataClient, AppConfig, Dialog , Scopes, SimplesGrid, XDataSource, AcessoService, Redirect, SimplesSelectBox) {
    //console.log('ApplicationController');
    //console.log($state.current.data.operacao);

    var urlAcesso = AppConfig.getAcessoUrl();
    console.log('Url Acesso 1',urlAcesso);

    //Menu UnidadeAdministrativa
    $rootScope.clienteListar = function(){
         $state.go('cliente', {entityName: 'Cliente'});
    };

    $scope.nomeSistema = AppConfig.getSistema();
    $scope.autenticado = AuthService.isAuthenticated();
    $scope.fieldLabelEmpresa = 'Empresa';

    var rootUrl = AppConfig.getRootUrl();
    console.log('Url Acesso 2',rootUrl);
    var EmpresaAtual =  $rootScope.EmpresaAtual = AuthService.getEmpresa();

    $scope.editarOrcamento = function(){
        var idExercicio = "39d82443-fd08-4654-a74d-0683a5653b46";
        XDataClient.uri = AppConfig.getRootUrl();
        XDataClient.list("Orcamento", "filter=Exercicio eq "+idExercicio).then(function(response){
            if (response.data.length > 0) {
                //console.log("Encontrado Orcamento");
                $state.go('tabela.editar', {entityName: 'Orcamento', entity: orcamento});
            }else{
                //console.log("Nenhum Orcamento");
                $state.go('tabela.incluir', {entityName: 'Orcamento', entity: {} });
            }
        });
    };

    HoraPublicacao();
    function HoraPublicacao(){
        XDataClient.uri = AppConfig.getAcessoUrl();
        console.log('CHEGUEI AQUi', XDataClient.uri);
        XDataClient.list('ParametroDocam').then(function(response){

          var _horaPub = response.data.value[0].HoraPublicacao.split(":");
          var _h = (_horaPub[0]);
          var _m = (_horaPub[1]);
          console.log('Tipo', _m);
          $rootScope.HoraPublicacaoApp = _h +':'+ _m;
          $rootScope.HPublicacao = _h;
          $rootScope.MPublicacao = _m;
        });
    }

    var PrefContainer = $rootScope.settings.layout.containerFluid = false;
    $rootScope.ajustarRecolucao = function(){
        if (PrefContainer) {
            PrefContainer = false;
        }else{
            PrefContainer = true;
        }
        $rootScope.settings.layout.containerFluid = PrefContainer;
    };

    $scope.visiblePopupEmp = false;

   function alterarEmpresa(){
        AcessoService.escolherEmpresa($scope.empresaSelecionada, AppConfig.getSistema()).then(function(res){
          Redirect.reloadApp();
        });
        return EscolherEmpresa;
    }

    $scope.showPopupEmpresa = function () {
        $scope.visiblePopupEmp = true;
        $scope.empresaSelecionada = $rootScope.EmpresaAtual.id;
    };

    $scope.visiblePopupEmp = false;
    $scope.PopupEmpresa = {
       width: 450,
       height: 200,
       contentTemplate: "infoEmpresa",
       showTitle: true,
       title: "Selecione uma empresa",
       closeOnOutsideClick: false,
       dragEnabled: true,
       toolbarItems: [
             {
                 toolbar: 'bottom', location: 'after', widget: 'button', options: {
                     text: 'Ok',
                     onClick: function (e) {
                        alterarEmpresa();
                        //EscolherEmpresa();
                     }
                 }
             }
        ],
       bindingOptions: {
           visible: "visiblePopupEmp",
       }
    };

   var selectEmpresa = new SimplesSelectBox(null,{
      bindingOptions: {
        dataSource: 'listaEmpresas',
        value: 'empresaSelecionada',
      },
      valueExpr: 'Id',
      displayExpr: 'RazaoSocial',
   });

   $scope.selectBoxEmpresa = selectEmpresa.options();

   montarSelectBoxEmpresa();
   function montarSelectBoxEmpresa(){
        AcessoService.getEmpresasCredenciadas().then(function(res){
            $scope.listaEmpresas = [];
            for (var i = 0; i < res.length; i++) {
                if (EmpresaAtual.id === res[i].EmpresaLicenciada.Id) {
                  $scope.empresaAtualCnpj = maskCnpj(res[i].EmpresaLicenciada.Cnpj);
                  //break;
                }
                $scope.listaEmpresas.push(res[i].EmpresaLicenciada);
            }
        }, function(error){
            //console.log(error);
        });
  }

    $scope.$on('$viewContentLoaded', function() {
        App.initComponents(); // init core components
    });

}]);

function maskCnpj(cnpj){
  return cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g,"\$1.\$2.\$3/\$4\-\$5");
}

/* Init global settings and run the app */
app.run(["$rootScope", "settings", "$state", "$location", "$window", "AuthService", "AUTH_EVENTS", "Redirect", function($rootScope, settings, $state, $location, $window, AuthService, AUTH_EVENTS, Redirect) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view

    $rootScope.$on(AUTH_EVENTS.expiredToken, function() {
        console.log('token expirado na aplicação. ', AuthService.isAuthenticated());
    });

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

    });



}]);
