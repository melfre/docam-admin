
angular.module('SimplesApp').controller('UnidadeAdministrativaController',
['$rootScope', '$scope', '$state', 'settings', '$http', '$q', '$stateParams', 'Dialog', 'NivelService', 'Scopes', 'AppConfig', 'XDataClient', '$filter',
function($rootScope, $scope, $state, settings, $http, $q, $stateParams, Dialog, NivelService, Scopes, AppConfig, XDataClient,  $filter) {

    Scopes.store('UnidadeAdministrativaController', $scope);
    $scope.setTitle('Orgão/Unidade Administrativa');
    $scope.setOperacao($state.current.data.operacao);
    $scope.setEntity($stateParams.entity || null);

    console.log($scope.jsonSchema);

    var _operacao = $state.current.data.operacao;
    var _entity = $stateParams.entity || null;


    //$scope.setEntity(_entity);
    $scope.expandEntity(['ParametroOrgao']).then(function(response){
        console.log('Response',response.ParametroOrgao);
        setLogomarcaAtualToImageGallery(response.ParametroOrgao);
        setMarcaDaguaAtualToImageGallery(response.ParametroOrgao);
    });

    $scope.setColumns([
        {dataField:'Codigo',width: '200', sortOrder: 'asc', customizeText: maskCodigoGrid},
        {dataField:'Descricao', caption: 'Descrição'},
        {dataField:'Sigla', width: '100', caption: 'Sigla'},
        // {dataField:'ServidorResponsavel.Nome', width: '300', caption: 'Responsável'},
    ]);

    function maskCodigoGrid(field){
        //console.log(field);
        var valor = field.value;
        if (valor.charAt(valor.length - 1) === '.') { // verifica se o último caractere é um ponto
            valor = valor.substring(0, valor.length - 1);
        }
        return valor;
    }

    $scope.codigo = '';
    function trataCodigo(e){
        var codigo = e.component._textValue;
        var codigoLength = codigo.length;
        var codigoSplit = null;
        codigoSplit = codigo.split(".");
        var codigoPrimeiroGrau = codigoSplit[0];

        codigoPrimeiroGrau = codigoPrimeiroGrau.replace("_", "");
        if (codigoPrimeiroGrau.length === codigoSplit[0].length) {
            //console.log(codigoPrimeiroGrau.length+"============"+codigoSplit[0].length);
            buscaNivel(codigoPrimeiroGrau);
        }
    }


    function substrMask(valor){
      //console.log('Valor mascara', valor);
        var caracterMask = "#";
        var resultado = '';
        var retorno = [];
        for (var i = 0; i < valor; i++) {
            resultado += '#';
        }
        retorno.push(resultado+'.');
      return retorno;
    }

    if ($state.current.data.operacao !== 'listar') {loadListNivel();}

    var mask = '';
    function loadListNivel(){
        NivelService.list().then(function(res){
          var formInstance = $scope.getFormInstance();
          $rootScope.nivel = res;
              if (res.data.value.length < 1) {
                  Dialog.confirm().title('Aviso').content('Não existe niveis cadastrados, deseja inserir!').show().then(function(){
                      $state.go('tabela.incluir', {entityName: 'Nivel', entity: {} });
                  }, function(){
                      $state.go('tabela.listar', {entityName: 'UnidadeAdministrativa', entity: {} });
                  });
              }else{
                  var valor = '';
                  for (var i = 0; i < res.data.value.length; i++) {
                     valor = valor.concat(substrMask(res.data.value[i].QtdDigito));
                  }
                  if (valor.charAt(valor.length - 1) === '.') { // verifica se o último caractere é um ponto
                      valor = valor.substring(0, valor.length - 1);
                  }
                  mask = valor;
                  formInstance.itemOption("Codigo", {
                      editorOptions:{
                        mask: mask,
                        useMaskedValue: true,
                        onKeyUp: trataCodigo,
                      }
                  });
              }
        });
    }


    $scope.imgPreviewLogomarca = [{}];
    $scope.galleryLogomarca = {
        bindingOptions: {
            dataSource: 'imgPreviewLogomarca'
        },
        height: 160,
        width: 300,
        showIndicator: false
    };

    $scope.imgPreviewMarcaDagua = [{}];
    $scope.galleryMarcaDagua = {
        bindingOptions: {
            dataSource: 'imgPreviewMarcaDagua'
        },
        height: 160,
        width: 300,
        showIndicator: false
    };

    $scope.valueAnexo = [];

    var token = AuthService.getToken();

    $scope.fileUploaderLogomarca = {
        labelText: "ou Arraste e Solte aqui",
        selectButtonText: "Selecione arquivo",
        accept: "image/jpg,image/png",
        uploadMode: "instantly",
        uploadHeaders: {
            Authorization: "Bearer " + token
        },
        uploadUrl: AppConfig.getUploadUrl() + "EnviarArquivoComum",
        showFileList: true,
        uploadedMessage: 'Arquivo Enviado',
        uploadFailedMessage: 'Arquivo Não Enviado',
        multiple: false,
        value: ['Logomarca'],
        onValueChanged: function (e) {
            $.each(e.value, function (_, value) {
                var fileURL = URL.createObjectURL(value);
                $scope.imgPreviewLogomarca = [];
                $scope.imgPreviewLogomarca.push({imageSrc: fileURL, name: value.name});
            });
            // debugger
        },
        onUploaded: function (e) {
            var response = JSON.parse(e.jQueryEvent.target.responseText);
            $scope.entity.ParametroOrgao.Logomarca = response;
        }
    };

    $scope.removerLogomarca = function () {
        $scope.imgPreviewLogomarca = [{}];
        $scope.entity.ParametroOrgao.Logomarca = null;
    };

    $scope.habilitarBotaoRemoverLogomarca = function () {
        var imgPreview = angular.copy($scope.imgPreviewLogomarca);
        if (imgPreview[0].imageSrc && imgPreview[0].imageSrc.length) {
            return true;
        }
        return false;
    };




    // Marca Dagua -----------------------------------------------------------------------------------------------------------------------
    $scope.fileUploaderMarcaDagua = {
        labelText: "ou Arraste e Solte aqui",
        selectButtonText: "Selecione arquivo",
        accept: "image/jpg,image/png",
        uploadMode: "instantly",
        uploadHeaders: {
            Authorization: "Bearer " + token
        },
        uploadUrl: AppConfig.getUploadUrl() + "EnviarArquivoComum",
        showFileList: true,
        uploadedMessage: 'Arquivo Enviado',
        uploadFailedMessage: 'Arquivo Não Enviado',
        multiple: false,
        value: ['Banner'],
        onValueChanged: function (e) {
            $.each(e.value, function (_, value) {
                var fileURL = URL.createObjectURL(value);
                $scope.imgPreviewMarcaDagua = [];
                $scope.imgPreviewMarcaDagua.push({imageSrc: fileURL, name: value.name});
            });
            // debugger
        },
        onUploaded: function (e) {
            var response = JSON.parse(e.jQueryEvent.target.responseText);
            $scope.entity.ParametroOrgao.MarcaDagua = response;
        }
    };

    $scope.removerBanner = function () {
        $scope.imgPreviewMarcaDagua = [{}];
        $scope.entity.ParametroOrgao.MarcaDagua = null;
    };

    $scope.habilitarBotaoRemoverMarcaDagua = function () {
        var imgPreview = angular.copy($scope.imgPreviewMarcaDagua);
        if (imgPreview[0].imageSrc && imgPreview[0].imageSrc.length) {
            return true;
        }
        return false;
    };

    function setLogomarcaAtualToImageGallery(entity) {
        if (entity !== null && typeof entity === "object") {
            if (entity["Logomarca@xdata.proxy"]) {
                var url1 = entity["Logomarca@xdata.proxy"];
                if (url1) {
                  console.log('Logo Marca', url1);
                    $http.get(AppConfig.getRootUrl() + url1).then(function (response) {
                        if (response.data) {
                            var value = angular.copy(response.data);
                            var _conteudo = value["Conteudo@xdata.proxy"];
                            if (_conteudo) {
                                $http.get(AppConfig.getRootUrl() + _conteudo, {responseType: 'arraybuffer'})
                                        .success(function (data) {
                                            var file = new Blob([data], {type: 'image/png'});
                                            var base64 = URL.createObjectURL(file);
                                            $scope.imgPreviewLogomarca = [];
                                            $scope.imgPreviewLogomarca.push({imageSrc: base64, name: "Logomarca"});
                                        });
                            }
                        }
                    });
                }
            }

            if (entity["Logomarca"]) {
                var url2 = entity["Logomarca"];
                if (url2) {
                    var _conteudo = url2["Conteudo@xdata.proxy"];
                    if (_conteudo) {
                        $http.get(AppConfig.getRootUrl() + _conteudo, {responseType: 'arraybuffer'})
                                .success(function (data) {
                                    var file = new Blob([data], {type: 'image/png'});
                                    var base64 = URL.createObjectURL(file);
                                    $scope.imgPreviewLogomarca = [];
                                    $scope.imgPreviewLogomarca.push({imageSrc: base64, name: "Logomarca"});
                                });
                    }
                }
            }
        }
    }

    function setMarcaDaguaAtualToImageGallery(entity) {
        if (entity !== null && typeof entity === "object") {
            if (entity["MarcaDagua@xdata.proxy"]) {
                var url1 = entity["MarcaDagua@xdata.proxy"];
                if (url1) {
                  console.log('Marca Dagua', url1);
                    $http.get(AppConfig.getRootUrl() + url1).then(function (response) {
                        if (response.data) {
                            var value = angular.copy(response.data);
                            var _conteudo = value["Conteudo@xdata.proxy"];
                            if (_conteudo) {
                                $http.get(AppConfig.getRootUrl() + _conteudo, {responseType: 'arraybuffer'})
                                        .success(function (data) {
                                            var file = new Blob([data], {type: 'image/png'});
                                            var base64 = URL.createObjectURL(file);
                                            $scope.imgPreviewMarcaDagua = [];
                                            $scope.imgPreviewMarcaDagua.push({imageSrc: base64, name: "MarcaDagua"});
                                        });
                            }
                        }
                    });
                }
            }

            if (entity["MarcaDagua"]) {
                var url2 = entity["MarcaDagua"];
                if (url2) {
                    var _conteudo = url2["Conteudo@xdata.proxy"];
                    if (_conteudo) {
                        $http.get(AppConfig.getRootUrl() + _conteudo, {responseType: 'arraybuffer'})
                                .success(function (data) {
                                    var file = new Blob([data], {type: 'image/png'});
                                    var base64 = URL.createObjectURL(file);
                                    $scope.imgPreviewMarcaDagua = [];
                                    $scope.imgPreviewMarcaDagua.push({imageSrc: base64, name: "MarcaDagua"});
                                });
                    }
                }
            }
        }
    }

    $scope.setFormOptions({
          colCount: 1,
          labelLocation: 'top',
          items: [
              {
                  itemType: 'group',
                  colCount: 10,
                  items: [
                      {
                          colSpan: 2,
                          dataField: 'Codigo',
                          label: {text: 'Código'},
                          editorOptions:{
                              //useMaskedValue: true,
                          },
                      },
                      {
                          colSpan: 6,
                          dataField: 'Descricao',
                          label:{text: 'Nome'}
                      },
                      {
                          colSpan: 2,
                          dataField: 'Sigla',
                          editorOptions: {
                              maxLength: 6,
                          }
                      },
                      {
                          colSpan: 2,
                          dataField: 'Fone1',
                          editorOptions: {
                              mask: "(00)0000.0000",
                          }
                      },
                      {
                          colSpan: 2,
                          dataField: 'Fone2',
                          editorOptions: {
                              mask: "(00)0000.0000",
                          }
                      },
                  ]
              },

              {
                  itemType: 'group',
                  colCount: 10,
                  caption: 'Endereço',
                  items: [
                      {
                          colSpan: 2,
                          dataField: 'Cep',
                          label: {text: 'Cep'},
                          editorOptions:{
                              mask: '00000-000'
                          },
                      },
                      {
                          colSpan: 6,
                          dataField: 'Logradouro',
                      },
                      {
                          colSpan: 2,
                          dataField: 'Bairro',
                          editorOptions: {

                          }
                      },
                      {
                          colSpan: 2,
                          dataField: 'Numero',
                          editorOptions: {

                          }
                      },
                      {
                          colSpan: 6,
                          dataField: 'Complemento',
                          editorOptions: {

                          }
                      },
                  ]
              },
              // {
              //     itemType: 'group',
              //     colCount: 2,
              //     caption: 'Parâmetros Documentos',
              //     items: [
              //         {
              //             itemType: 'group',
              //             colCount: 1,
              //             items: [
              //                 {
              //
              //                 }
              //             ]
              //         }
              //     ]
              // },
              {
                  itemType: 'group',
                  colCount: 10,
                  caption: 'Parâmetros Documentos',
                  items: [
                      {
                          colSpan: 2,
                          label: {text: 'Numero Proximo Oficio'},
                          dataField: 'ParametroOrgao.CodigoInicioOficio',

                          // editorOptions:{
                          //     mask: '00000-000'
                          // },
                      },
                      {
                          colSpan: 2,
                          label: {text: 'Codigo Inicio Memorando'},
                          dataField: 'ParametroOrgao.CodigoInicioMemorando',
                          // editorOptions:{
                          //     mask: '00000-000'
                          // },
                      },

                  ]
              },

              {
                  itemType: "tabbed",
                  tabs: [
                      {
                          title: "Logomarca",
                          items: [
                              {
                                  dataField: "ParametroOrgao.Logomarca",
                                  template: "divLogomarca",
                                  validationRules: [{
                                          type: "required",
                                          message: "Logomarca requerida"
                                      }]
                              }
                          ]
                      },
                      {
                          title: "Marca D'agua",
                          items: [
                              {
                                  dataField: "ParametroOrgao.MarcaDagua",
                                  template: "divMarcaDagua",
                                  validationRules: [{
                                          type: "required",
                                          message: "Banner requerido"
                                      }]}]
                      }
                  ]
              },

          ]
      });



      if ($state.current.data.operacao === 'incluir') {
          $scope.setEntity({ParametroOrgao: {}});
      }

}]);
