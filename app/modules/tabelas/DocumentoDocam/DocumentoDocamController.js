angular.module('SimplesApp').controller('DocumentoDocamController', ['$rootScope', '$scope', '$state', 'settings', '$http', '$q', '$stateParams', 'Dialog', 'Scopes', 'AppConfig', 'AuthService', 'CrudService', '$filter', 'XDataClient', 'DocumentoDocamService',
    function($rootScope, $scope, $state, settings, $http, $q, $stateParams, Dialog, Scopes, AppConfig, AuthService, CrudService, $filter, XDataClient, DocumentoDocamService) {

        Scopes.store('DocumentoDocamController', $scope);
        $scope.setTitle('Documentos Oficiais');
        $scope.setOperacao($state.current.data.operacao);
        $scope.setEntity($stateParams.entity || null);

        var rootUrl = AppConfig.getRootUrl();

        var now = new Date();
        var nowPublicacao = new Date();
        var horaAtual = now.getHours();
        var minutosAtual = now.getMinutes();

        //Horario a ser definido nos parametros pela ABRACAM
        var horaLimitePublicacao;

        var listTipoDocumento = new DevExpress.data.DataSource({
            load: function(loadOptions) {
                return DocumentoDocamService.getTipos().then(function(response) {
                    return response.data.value;
                });
            },
            byKey: function(key) {
                return key;
            }
        });

        $scope.nomeArquivo = '';
        $scope.dataSourceArquivo = [{}];
        $scope.dataPublicacao = undefined;

        if ($state.current.data.operacao === 'incluir') {
            //var formInstance = $scope.getFormInstance();
            //verificaHoraServidor(formInstance);
        }

        $scope.expandEntity(['Arquivo']).then(function(response){
            $scope.dataSourceArquivo = response;
            $scope.nomeArquivo = response.Arquivo.NomeArquivo;

            var dataEnvio = response.DataEnvio;
            var dataPublicacao = response.DataPublicacao;

            var formInstance = $scope.getFormInstance();

            formInstance.itemOption("DataEnvio", {
                editorOptions:{
                    type: 'date',
                    width: '100%',
                    value: dataEnvio,
                    displayFormat: function(value) {
                        return $filter('date')(new Date(value), 'dd/MM/yyyy', '+300');
                    },
                    disabled: true,
                }
            });


            // formInstance.itemOption("DataPublicacao", {
            //     editorOptions:{
            //       type: 'date',
            //       width: '100%',
            //       value: dataPublicacao,
            //       displayFormat: function(value) {
            //           return $filter('date')(new Date(value), 'dd/MM/yyyy', '+300');
            //       },
            //     },
            // });

            verificaHoraServidor(formInstance);
        });

        $scope.setColumnActions(ActionGrid());
        $scope.setColumns([
            {
                dataField: 'Status',
                width: 120,
                cellTemplate: function(container, options) {
                    //                    console.log(options.data);
                    if (options.data.Status === 'Enviado') {
                        $('<span class="badge badge-default bg-purple" style="width:100%"; padding:5px;> <b>Enviado</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'Processando') {
                        $('<span class="badge badge-primary bg-yellow-casablanca" style="width:100%"> <b>Processando</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'Processada') {
                        $('<span class="badge badge-success bg-blue-madison" style="width:100%"> <b>Processada</b></span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'Publicada') {
                        $('<span class="badge badge-success bg-green-meadow" style="width:100%"> <b>Publicada</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'NaFila') {
                        $('<span class="badge badge-default bg-blue-soft" style="width:100%"> <b>Na Fila</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'RetiradoDaFila') {
                        $('<span class="badge badge-danger" style="width:100%"> <b>Retirado da Fila</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'ComErro') {
                        $('<span class="badge badge-danger" style="width:100%"> <b>Com Erro</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'Cancelada') {
                        $('<span class="badge badge-danger" style="width:100%"> <b>Cancelada</b> </span>')
                            .appendTo(container);
                    }
                }
            },
            { dataField: 'DocumentoTipo.Descricao', caption: 'Tipo' },
            { dataField: 'Descricao', caption: 'Descricao' },
            {
                dataField: 'DataEnvio',
                width: '150',
                caption: 'Dta. Envio',
                customizeText: function(cellInfo) {
                    return convertDate(cellInfo.value);
                }
            },
            {
                dataField: 'DataPublicacao',
                width: '150',
                caption: 'Dta. Publicação',
                customizeText: function(cellInfo) {
                    return convertDate(cellInfo.value);
                }
            },
            {
                dataField: 'CodigoIdentificador',
                width: '150',
                caption: 'Identificador',
                // customizeText: function(cellInfo) {
                //     return convertDate(cellInfo.value);
                // }
            },
            {
                caption: "Arquivo",
                dataField: "Arquivo",
                width: 100,
                cellTemplate: function(container, options) {
                    var arquivo = options;
                    if (options.data.Arquivo !== null) {
                        $('<a class="btn btn-xs green-meadow" data-original-title=""><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Arquivo</a>')
                            .on('dxclick', function() {
                                getArquivo(options.data.Id);
                                //verArquivo(options.data.Id);

                            })
                            .appendTo(container);
                    }

                }
            }
        ]);

        $scope.gridSettings.export.fileName = "Leis - Decretos";

        $scope.arquivoLegislacao = '';

        function ActionGrid() {
            return {
                name: 'actionColumn',
                fixed: true,
                fixedPosition: 'right',
                caption: 'Ações',
                width: 100,
                alignment: 'center',
                cellTemplate: function (container, options) {
                    $('<a class="tooltips btn btn-xs default" data-container="body" data-placement="top" data-original-title="Visualizar"><i class="fa fa-search"></i></a>')
                    .on('dxclick', function () { $scope.ver(options.data); })
                    .appendTo(container);

                    if (options.data.Status === 'Publicados') {
                        $('<a class=" btn btn-xs default disabled" data-container="body" style="margin-left: 5px" si-tooltip="Editar"><i class="fa fa-edit"></i></a>')
                        .on('dxclick', function () { $scope.editar(options.data); })
                        .appendTo(container);
                    }else{
                        $('<a class=" btn btn-xs blue" data-container="body" style="margin-left: 5px" si-tooltip="Editar"><i class="fa fa-edit"></i></a>')
                        .on('dxclick', function () { $scope.editar(options.data); })
                        .appendTo(container);
                    }
                }
            };
        }

        function convertDate(date) {
            if (date) {
                return $filter('date')(new Date(date), "dd/MM/yyyy", '+300');
            }
        }

        function convertDataHora(date) {
            if (date) {
                return $filter('date')(new Date(date), "dd/MM/yyyy HH:mm:ss");
            }
        }

        var _desabilitaDatas;


        function verificaHoraServidor(formInstance){
            var horaParametro = $rootScope.HPublicacao;
            var minutoParametro = $rootScope.MPublicacao;
            var setData = new Date();

            //console.log('Hora Atual', horaAtual+':'+minutosAtual);

            //console.log('Hora Publicacao', horaParametro+ ':' +minutoParametro);

            if (horaAtual >= horaParametro) {
                if (minutosAtual >= minutoParametro) {
                    nowPublicacao =  setData.setDate(now.getDate() + 1);
                    var formInstance = $scope.getFormInstance();
                    //console.log('Instancia do form', formInstance)
                    formInstance.itemOption("DataPublicacao", {
                        editorOptions:{
                          type: 'date',
                          width: '100%',
                          value: nowPublicacao,
                          displayFormat: function(value) {
                              return $filter('date')(new Date(value), 'dd/MM/yyyy', '+300');
                          },
                        },
                    });
                }
            }else{
                nowPublicacao = now;
            }
        }



        function verArquivo(idContrato) {
            RelatorioService.getArquivo(idContrato);
        }

        function getArquivo(id) {

            XDataClient.get('DocumentoDocam', id, 'Arquivo').then(function(response) {
                var tipoArquivo = response.data.Arquivo.ExtensaoArquivo;
                var _nomeArquivo = response.data.Arquivo.NomeArquivo;
                var _tipoArquivo = null;

                if (tipoArquivo === ".pdf") {
                    _tipoArquivo = 'application/pdf';
                }

                if (tipoArquivo === ".xlsx") {
                    //_tipoArquivo = 'application/vnd.ms-excel';
                    _tipoArquivo = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                }

                if (tipoArquivo === ".xls") {
                    //_tipoArquivo = 'application/vnd.ms-excel';
                    _tipoArquivo = 'application/excel';
                }

                if (tipoArquivo === ".docx") {
                    _tipoArquivo = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                }

                if (tipoArquivo === ".word") {
                    _tipoArquivo = 'application/msword';
                }

                if (response.data.Arquivo) {

                    $http.get(rootUrl + "Arquivo('" + response.data.Arquivo.Id + "')/Conteudo", { responseType: 'arraybuffer' })
                    .then(function(resp) {
                        var file = new Blob([resp.data], { type: _tipoArquivo });
                        var fileURL = (window.URL || window.webkitURL).createObjectURL(file);
                        var link = angular.element('<a/>');
                         link.attr({
                             href : fileURL,
                             download : _nomeArquivo
                         })[0].click();
                        //window.open(fileURL);
                    });

                }

            });
        }

        var token = AuthService.getToken();

        $scope.fileUploaderArquivo = {
            labelText: "ou Arraste e Solte aqui",
            dataField: "Arquivo",
            selectButtonText: "Anexar Arquivo PDF",
            accept: "application/pdf",
            uploadMode: "instantly",
            uploadHeaders: {
                Authorization: "Bearer " + token,
            },
            showFileList: true,
            uploadedMessage: 'Arquivo Enviado',
            uploadFailedMessage: 'Arquivo Não Enviado',
            uploadUrl: AppConfig.getUploadUrl() + "EnviarArquivoComum",
            // uploadUrl: "http://localhost:2002/EnviarArquivoComum",
            multiple: false,
            onValueChanged: function(e) {

            },
            onUploaded: function(e) {
                var response = JSON.parse(e.jQueryEvent.target.responseText);
                $scope.entity.Arquivo = response;
                $scope.nomeArquivo = response.NomeArquivo;
            }
        };

        $scope.onInserting = function(entity) {
            verificaHoraServidor();
            entity.DataEnvio = $filter('date')(new Date(entity.DataEnvio), "yyyy-MM-dd", '+300');
            entity.DataPublicacao = $filter('date')(new Date(entity.DataPublicacao), "yyyy-MM-dd", '+300');
        };

        $scope.onModifying = function(entity) {
            verificaHoraServidor();
            //console.log('Data Publicacao Modificada', entity.Publicacao);
            entity.DataEnvio = $filter('date')(new Date(entity.DataEnvio), "yyyy-MM-dd", '+300');
            entity.DataPublicacao = $filter('date')(new Date(entity.DataPublicacao), "yyyy-MM-dd", '+300');
        };


        $scope.setFormOptions({
            colCount: 1,
            // bindingOptions: {
            //     formData: "entity"
            // },
            labelLocation: 'top',
            items: [
                {
                    itemType: 'group',
                    colCount: 10,
                    items: [
                      {
                          colSpan: 3,
                          dataField: 'DocumentoTipo',
                          label: { text: 'Tipo' },
                          isRequired: true,
                          editorOptions: {
                              bindingOptions: {
                                  dataSource: 'listTipoDocumento',
                                  value: 'entity.TipoDocumento'
                              },
                              searchExpr: 'Descricao',
                              displayExpr: 'Descricao',
                              placeholder: 'Selecione...',
                              searchEnabled: true
                          }
                      },
                        {
                            colSpan: 2,
                            dataField: 'DataEnvio',
                            label: { text: 'Data Envio' },
                            editorOptions: {
                                type: 'date',
                                width: '100%',
                                displayFormat: function(value) {
                                    return $filter('date')(new Date(value), 'dd/MM/yyyy', '+300');
                                },
                                value: now,
                                disabled: true,
                            }
                        },
                        {
                            colSpan: 2,
                            dataField: 'DataPublicacao',
                            label: { text: 'Data da Publicação' },
                            editorOptions: {
                                type: 'date',
                                width: '100%',
                                displayFormat: function(value) {
                                    return $filter('date')(new Date(value), "dd/MM/yyyy", '+300');

                                },
                                //value: nowPublicacao,
                                disabledDates: _desabilitaDatas,

                                onValueChanged: function (e) {
                                    var data_sel = $filter('date')(new Date(e.value), "dd/MM/yyyy", '+300');
                                    //
                                    // if ($filter('date')(new Date(now), "dd/MM/yyyy", '+300') == data_sel) {
                                    //     //console.log('Datas iguais');
                                    //     Dialog.alert().title('Datas iguais!').warning().show();
                                    // }

                                    if ($filter('date')(new Date(now), "dd/MM/yyyy", '+300') > data_sel) {
                                        Dialog.alert().title('Aviso').content('Data Publicação não pode ser menor que Data Envio!').warning().show();
                                        //DevExpress.ui.dialog.alert('Data Publicação não pode ser menor que Data Envio!');
                                    }
                                }

                            },


                        }
                    ]
                },
                {
                    itemType: 'group',
                    colCount: 10,
                    items: [{
                        colSpan: 10,
                        dataField: 'Descricao',
                        label: { text: 'Descrição' },
                        editorType: 'dxTextArea',
                        editorOptions: {
                            width: '100%',
                            height: '70',
                            maxLength: '255',
                            onKeyUp: function(e) {
                                //console.log('UP', e);
                                //console.log('UP', e.component._textValue);
                            },
                            // onKeyPress: function (e) {
                            //   //return
                            //    console.log('PRESS', e.component._textValue);
                            //    return e
                            // }
                        },

                    }]
                },
                {
                    itemType: 'group',
                    colCount: 10,
                    items: [{
                        label: {},
                        dataField: 'Arquivo',
                        colSpan: 10,
                        template: 'fileArquivo'
                            // validationRules: [
                            //     {
                            //         type: "required",
                            //         message: "Favor Adicione um arquivo"
                            //     }
                            // ]
                    }]
                }
            ]
        });
    }
]);
