angular.module('SimplesApp').factory('DocumentoDocamService', ['$http', 'XDataClient', '$q', '$window', 'AppConfig', function($http, XDataClient, $q, $window, AppConfig) {

    var service = this;
    var rootUrl = AppConfig.getRootUrl();
    //var _entityName = '';

    this.getCountAll = getCountAll;
    this.getArquivo = getArquivo;
    this.getTipos = getTipos;
    //console.log(rootUrl);

    return this;

    function getCountAll(entityName) {
        var url = rootUrl + entityName + "service/list?$top=0&$inlinecount=allpages";
        return $http.get(url);
    }

    function getArquivo(idLegislacao) {

        XDataClient.get('DocumentoDocam', idLegislacao, 'Arquivo').then(function(response) {

            if (response.data.Arquivo) {
                $http.get(rootUrl + "Arquivo('" + response.data.Arquivo.Id + "')/Conteudo", { responseType: 'arraybuffer' })
                    .then(function(data) {
                        var file = new Blob([data], { type: 'application/pdf' });
                        var fileURL = URL.createObjectURL(file);
                        //if (autoLoad === true){
                            $window.location.href = fileURL;
                        //}
                    });
            }

        });
    }

    function getTipos() {
        return $http.get(rootUrl + "TipoDocumento?$orderby=Nome");
    }
}]);
