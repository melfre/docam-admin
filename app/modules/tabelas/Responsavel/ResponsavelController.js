
angular.module('SimplesApp').controller('ResponsavelController',
['$rootScope', '$scope', '$state', 'settings', '$http', '$q', '$stateParams', 'Dialog', 'Scopes', 'AppConfig', '$filter',
function($rootScope, $scope, $state, settings, $http, $q, $stateParams, Dialog, Scopes, AppConfig, $filter) {

    Scopes.store('ResponsavelController', $scope);
    $scope.setTitle('Responsavel');
    $scope.setOperacao($state.current.data.operacao);
    $scope.setEntity($stateParams.entity || null);
    //$scope.setEntity({Pessoa:{}});
    //$scope.setverActions(true);

    $scope.tinymceOptions = {
      plugins: 'link image code table',
      toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table'
    };

    console.log($scope.jsonSchema);

    $scope.expandEntity(['Pessoa']);

    $scope.setColumns([
        {dataField:'Nome', caption: 'Nome'},
        {dataField:'Cpf', width: '160',caption: 'Cpf', customizeText: maskCpf},

    ]);

    function maskCpf(field){
        return field.value.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g,"\$1.\$2.\$3-\$4");
    }

    var formOptions = {
        colCount: 1,
        labelLocation: 'left',
        items: [
            {
                itemType: 'group',
                colCount: 10,
                items: [
                    {
                        colSpan: 2,
                        dataField: 'Cpf',
                        label:{text: 'Cpf'},
                        editorOptions: {
                            mask: "000.000.000-00",
                            maskRules: {"X": /[02-9]/},
                            maxLength: 11,
                        }
                    },
                ]
            },
            {
                itemType: 'group',
                colCount: 10,
                items: [
                    {
                        colSpan: 4,
                        dataField: 'Nome',
                        label: {text: 'Nome'},

                    },
                ]
            },


        ]
    };
    $scope.setFormOptions(formOptions);
}]);
