
angular.module('SimplesApp').controller('TipoDocumentoController',
['$rootScope', '$scope', '$state', 'settings', '$http', '$q', '$stateParams', 'Dialog', 'Scopes', 'AppConfig', '$filter',
function($rootScope, $scope, $state, settings, $http, $q, $stateParams, Dialog, Scopes, AppConfig, $filter) {

    Scopes.store('TipoDocumentoController', $scope);
    $scope.setTitle('Tipos de Documento');
    $scope.setOperacao($state.current.data.operacao);
    $scope.setEntity($stateParams.entity || null);

    $scope.setColumnActions(ActionVisualizar());
    $scope.setColumns([
        {dataField:'Descricao', caption : 'Nome'},
        {dataField:'Padrao', caption : 'Padrão', width: 100}
    ]);

    function ActionVisualizar() {
        return {
            name: 'actionColumn',
            fixed: true,
            fixedPosition: 'right',
            caption: 'Ações',
            width: 100,
            alignment: 'center',
            cellTemplate: function (container, options) {
                $('<a class="tooltips btn btn-xs default" data-container="body" data-placement="top" data-original-title="Visualizar"><i class="fa fa-search"></i></a>')
                .on('dxclick', function () { $scope.ver(options.data); })
                .appendTo(container);

                if (options.data.Padrao) {
                    $('<a class=" btn btn-xs default disabled" data-container="body" style="margin-left: 5px" si-tooltip="Editar"><i class="fa fa-edit"></i></a>')
                    .on('dxclick', function () { $scope.editar(options.data); })
                    .appendTo(container);
                }else{
                    $('<a class=" btn btn-xs blue" data-container="body" style="margin-left: 5px" si-tooltip="Editar"><i class="fa fa-edit"></i></a>')
                    .on('dxclick', function () { $scope.editar(options.data); })
                    .appendTo(container);
                }
            }
        };
    }
    
    var formOptions = {
        colCount: 1,
        labelLocation: 'left',
        items: [
            {
                itemType: 'group',
                colCount: 10,
                items: [
                    {
                        colSpan: 4,
                        dataField: 'Descricao',
                        label: {text: 'Nome'}
                    },
                ]
            },
        ]
    };

    $scope.onInserting = function(entity) {
        entity.Padrao = false;
    }

    $scope.setFormOptions(formOptions);

}]);
