//angular.module('scp.preferencias', []);
angular.module('SimplesApp')
.factory('PreferenciaUsuarioService', ['$rootScope','$http', '$q', 'XDataClient', 'Dialog', 'AppConfig', 
function ($rootScope, $http, $q, XDataClient, Dialog, AppConfig) {

     
     
     //this.getExercicio = getExercicio;
     //this.setExercicio = setExercicio;
     this.abrirNovoExercicio = abrirNovoExercicio;
     this.getMinhasPreferencias = getMinhasPreferencias;    
     this.alterarCompetencia = alterarCompetencia; 
     return this;
     
     function abrirNovoExercicio(novoExercicio){
          var rootUrl = AppConfig.getRootUrl();
          //console.log("Ano do exericio Atual", novoExercicio, rootUrl);
         return $http.post(rootUrl + "ExercicioService/AbrirNovoExercicio", novoExercicio);
    }

    //GET - UsuarioPreferenciaService/MinhasPreferencias
    //Pode vir error 404 (ignorar)
    function getMinhasPreferencias(){
          var rootUrl = AppConfig.getRootUrl();
          return $http.get(rootUrl + "UsuarioPreferenciaService/MinhasPreferencias");
    }

    //POST - UsuarioPreferenciaService/AlterarCompetencia(ano=inteiro, mes=inteiro)
    function alterarCompetencia(competencia){
          var rootUrl = AppConfig.getRootUrl();
          var competencia = competencia;
          return $http.post(rootUrl + 'UsuarioPreferenciaService/AlterarCompetencia', competencia);
    }
   



}]);
