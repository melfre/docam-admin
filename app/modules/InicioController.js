angular.module('SimplesApp').controller('InicioController',
    ['$rootScope', '$scope', '$state', 'settings', '$http', '$q', '$filter', '$stateParams', 'Dialog', 'Scopes', 'XDataClient', 'InicioService', 'SimplesGrid', 'SimplesLookup', 'SimplesSelectBox', 'XDataSource', 'AppConfig', 'Utils','AcessoService',
    //['$rootScope', '$scope', '$state', 'XDataClient' ,'InicioService' , 'XDATA_CONSTANTS', 'Scopes',
    function ($rootScope, $scope, $state, settings, $http, $q, $filter, $stateParams, Dialog, Scopes, XDataClient, InicioService, SimplesGrid, SimplesLookup, SimplesSelectBox, XDataSource, AppConfig, Utils, AcessoService) {

    var rootUrl = AppConfig.getRootUrl();
    var dataSource = new XDataSource();

    //console.log(rootUrl+'ParametroDocam');

    dataSource.setUri(rootUrl);
    dataSource.setEntityName('DocumentoDocam');
    dataSource.setExpand('DocumentoTipo');


    var grid = new SimplesGrid(dataSource);

    loadComponents();
    function loadComponents() {

        $scope.gridDocumentosSettings = grid.settings();
        $scope.gridDocumentosSettings.columns = [

            {
                dataField: 'Status',
                width: 120,
                cellTemplate: function(container, options) {
                    //                    console.log(options.data);
                    if (options.data.Status === 'Nova' || options.data.Status === 'Enviado') {
                        $('<span class="badge badge-default bg-purple" style="width:100%"; padding:5px;> <b>Enviado</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'Processando') {
                        $('<span class="badge badge-primary bg-yellow-casablanca" style="width:100%"> <b>Processando</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'Processada') {
                        $('<span class="badge badge-success bg-blue-madison" style="width:100%"> <b>Processada</b></span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'Publicada') {
                        $('<span class="badge badge-success bg-green-meadow" style="width:100%"> <b>Publicada</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'NaFila') {
                        $('<span class="badge badge-default bg-blue-soft" style="width:100%"> <b>Na Fila</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'RetiradoDaFila') {
                        $('<span class="badge badge-danger" style="width:100%"> <b>Retirado da Fila</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'ComErro') {
                        $('<span class="badge badge-danger" style="width:100%"> <b>Com Erro</b> </span>')
                            .appendTo(container);
                    } else if (options.data.Status === 'Cancelada') {
                        $('<span class="badge badge-danger" style="width:100%"> <b>Cancelada</b> </span>')
                            .appendTo(container);
                    }
                }
            },
            { dataField: 'DocumentoTipo.Descricao', caption: 'Tipo' },
            { dataField: 'Descricao', caption: 'Descricao' },
            {
                dataField: 'DataEnvio',
                width: '150',
                caption: 'Dta. Envio',
                customizeText: function(cellInfo) {
                    return convertDate(cellInfo.value);
                }
            },
            {
                dataField: 'DataPublicacao',
                width: '150',
                caption: 'Dta. Publicação',
                customizeText: function(cellInfo) {
                    return convertDate(cellInfo.value);
                }
            },
            {
                caption: "Arquivo",
                dataField: "Arquivo",
                width: 100,
                cellTemplate: function(container, options) {
                    var arquivo = options;
                    if (options.data.Arquivo !== null) {
                        $('<a class="btn btn-xs green-meadow" data-original-title=""><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Arquivo</a>')
                            .on('dxclick', function() {
                                getArquivo(options.data.Id);
                                //verArquivo(options.data.Id);

                            })
                            .appendTo(container);
                    }

                }
            }
        ];

    }

    $scope.horaPublicacao = undefined;

    getHora();
    function getHora(){
        InicioService.getParametroDocam().then(function(response){
            var _horaPub = response.data.value[0].HoraPublicacao.split(":");
            var _h = parseInt(_horaPub[0]);
            var _m = parseInt(_horaPub[1]);
            // console.log('dkljajkhfhaskjfhjaskhfjhaskjfhjksahfjkdhasjkfhjkasdhj');
            // console.log('Tipo', typeof _m);
            $scope.horaPublicacao = _h +':'+ _m;
            //console.log('Tipo hora e minuto', typeof _h + ' --- ' + typeof _m);
            //console.log('Resposta', response.data.value[0].HoraPublicacao);
            //$scope.countPublicados = res;
        });
    }

    function convertDate(date) {
        if (date) {
            return $filter('date')(new Date(date), "dd/MM/yyyy", '+300');
        }
    }

    function verArquivo(idContrato) {
        RelatorioService.getArquivo(idContrato);
    }

    function getArquivo(id) {

        XDataClient.get('DocumentoDocam', id, 'Arquivo').then(function(response) {
            var tipoArquivo = response.data.Arquivo.ExtensaoArquivo;
            var _nomeArquivo = response.data.Arquivo.NomeArquivo;
            var _tipoArquivo = null;

            if (tipoArquivo === ".pdf") {
                _tipoArquivo = 'application/pdf';
            }

            if (tipoArquivo === ".xlsx") {
                //_tipoArquivo = 'application/vnd.ms-excel';
                _tipoArquivo = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            }

            if (tipoArquivo === ".xls") {
                //_tipoArquivo = 'application/vnd.ms-excel';
                _tipoArquivo = 'application/excel';
            }

            if (tipoArquivo === ".docx") {
                _tipoArquivo = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
            }

            if (tipoArquivo === ".word") {
                _tipoArquivo = 'application/msword';
            }

            if (response.data.Arquivo) {

                $http.get(rootUrl + "Arquivo('" + response.data.Arquivo.Id + "')/Conteudo", { responseType: 'arraybuffer' })
                .then(function(resp) {
                    var file = new Blob([resp.data], { type: _tipoArquivo });
                    var fileURL = (window.URL || window.webkitURL).createObjectURL(file);
                    var link = angular.element('<a/>');
                     link.attr({
                         href : fileURL,
                         download : _nomeArquivo
                     })[0].click();
                    //window.open(fileURL);
                });

            }

        });
    }

    var queryStatusPublicado = '&$filter=(Status eq ' + 5 + ')';
    countDocPulicado('DocumentoDocam', queryStatusPublicado);
    function countDocPulicado(entidade, query){
        InicioService.countAll(entidade, query).then(function(res){
            //console.log('Resposta', res);
            $scope.countPublicados = res;
        });
    }

    var queryStatusNaoPublicado = '&$filter=(Status ne ' + 5 + ')';
    countNaoDocPulicado('DocumentoDocam', queryStatusNaoPublicado);
    function countNaoDocPulicado(entidade, query){
        InicioService.countAll(entidade, query).then(function(res){
            //console.log('Resposta', res);
            $scope.countNaoPublicado = res;
        });
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();

        //getCountFuncionarios('SfpSyncFuncionario');

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });
}]);
