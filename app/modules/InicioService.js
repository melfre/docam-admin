angular.module('SimplesApp').factory('InicioService', ['$http', 'XDataClient', '$q', 'AppConfig', function ($http, XDataClient, $q, AppConfig) {

    var service = this;
    var rootUrl = AppConfig.getRootUrl();
    //var _entityName = '';

    this.getCountAll = getCountAll;
    this.countAll = countAll;
    this.getParametroDocam = getParametroDocam;
    //console.log(rootUrl);

    return this;

    function getCountAll(entityName){
      return XDataClient.count(entityName);
    }

    function countAll(entityName, query){
      var queryDefault = '$inlinecount=allpages&$top=0';
      var url = rootUrl + entityName +'?'+ queryDefault+query;
      return $q(function(resolve, reject){
          $http.get(url).then(function(res){
              resolve(res.data['@xdata.count']);
          }, function(res){
              reject(res);
          });
      });
    }

    function getParametroDocam(){
      //return XDataClient.list(rootUrl+'ParametroDocam');
      //var url = rootUrl+'ParametroDocam';
      return $http.get("http://localhost:2002/docam/acesso/ParametroDocam");
    }

}]);
