/* Setup Rounting For All Pages */

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/");

    $stateProvider

        .state('inicio', {
            url: "/",
            templateUrl: "app/modules/inicio.html",
            data: {pageTitle: 'Início'},
            controller: "InicioController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'SimplesApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [

                            'app/modules/InicioService.js',
                            'app/modules/InicioController.js',
                        ]
                    }]);
                }]
            }

        })


        .state('UnidadeAdministrativa', {
            url: "/UnidadeAdministrativa",
            templateUrl: "app/modules/tabelas/UnidadeAdministrativa/unidadeadministrativa.html",
            //data: {pageTitle: 'Minha conta'},
            controller: "MinhaContaController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'SimplesApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                          //  '../assets/pages/css/profile.min.css',
                          //  '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //'app/modules/tabelas/MinhaConta/MinhaContaController.js',
                        ]
                    }]);
                }],
                // minhaconta: ['MinhaContaService', function(MinhaContaService) {
                //     return MinhaContaService.minhaConta().then(function(res) {
                //         return res.data;
                //     });
                // }]

            }
        });

      //  UnidadeAdministrativa



}]);
