// Configurando mensagens de diálogo
app.config(function(DialogConfigProvider) {
    DialogConfigProvider.setConfig(
        {
            alert: { title: "Docam-Admin" }
        }
    );
});

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
app.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

// Configurando Jwt
app.config(function(AppConfigProvider) {
  //AppConfigProvider.setRootUrl("http://192.168.1.66:2002/:slug/cpl/");
  //AppConfigProvider.setRootUrl("https://xdata.simplesi.com.br/:slug/scp/");
  //AppConfigProvider.setRootUrl("http://localhost/angular-ceke-angular/api/");
  AppConfigProvider.setRootUrl("http://localhost:2002/:slug/docam-admin/");
  //AppConfigProvider.setTokenId('Administracao-b7VudHJhbGRvdXN1YwuXerbwu==');
  AppConfigProvider.setTokenId('SimplesApp-b7VudHJhbGRvdXN1YXJpdoc==');
  AppConfigProvider.setSistema('Docam-Admin');
  AppConfigProvider.setIsLoginApp(false);
});
